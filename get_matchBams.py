import sys

_in = open(sys.argv[1])
_out = open(sys.argv[2],"w")

_in.readline()

print >> _out, "Patient\ttumorKey\ttumorFile\tcontrolKey\tcontrolFile"
sm_pair = {} 
danyang_normal = {} # patient -> normal path
danyang_cancer = {} # sample -> path
for line in _in:
    line = [x.strip('"') for x in line.strip().split('\t')]
    key = line[0].split('_')        
    if key[0] in ["OC029","OC022"]:
        if key[-1] == "normal":
            danyang_normal[key[0]] = line[1]
        else: 
            danyang_cancer["_".join(key)]  = line[1]
    else:        
        sm = '_'.join(key[:-1])
        sm_pair.setdefault(sm,[]).append(line[1])
_in.close()

for k,v in sm_pair.items():
    print >> _out, k.split('_')[0]+'\t'+k+'_tumor\t'+v[0]+'\t'+k+'_normal\t'+v[1]   
for s,p in danyang_cancer.items():
    patient = s.split("_")[0]
    print >> _out, patient+"\t"+s+"\t"+p+"\t"+s.replace("cancer","normal")+"\t"+danyang_normal[patient]

_out.close()    
