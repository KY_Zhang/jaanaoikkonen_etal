#!/usr/bin/env anduril
//$OPT --threads 50
//$OPT --pipe "tee $ANDURIL_EXECUTION_DIR/_log"
//$OPT --pipe "anduril-pager --ls"
//$OPT --wrapper slurm-prefix

import anduril.builtin._
import anduril.tools._
import anduril.anima._
import anduril.microarray._
import anduril.sequencing._
import org.anduril.runtime._

object createPON{

    val reference = INPUT(path = "GRCh38.d1.vd1.fa")
    val intervals = INPUT(path = "targetGenes.interval_list")
   	val dbsnp = INPUT(path = "dbsnp_144_sorted.hg38_withChrSorted.vcf")
    val cosmic = INPUT(path = "cosmic79_4mutect_sorted.vcf") 
    val batch1 = INPUT(path = "bams1.csv")
    val batch2 = INPUT(path = "bams2.csv)
	val varsPerNormal = NamedMap[BashEvaluate]("varsPerNormal")
	val varsPerNormalVCF = NamedMap[BinaryFile]("varsPerNormalVCF")
    val join = CSVJoin(in1 = batch1,
                       in2 = batch2,
                       intersection = false)
    val normals = CSVFilter(in = join.out,
                            regexp = "Key=^.*normal$")
    
    for (row <- iterCSV(normals.out)){
		val key = row("Key")
		val bam = INPUT(row("File"))
		varsPerNormal(key) = BashEvaluate(var1 = reference,
										  var2 = bam,
										  var3 = cosmic,
										  var4 = dbsnp,
										  var5 = intervals,
										  script = """ java -Xmx4g -jar /opt/share/gatk-3.7/GenomeAnalysisTK.jar -T MuTect2 -R @var1@ -I:tumor @var2@ --dbsnp @var4@ --cosmic @var3@ --artifact_detection_mode -L @var5@ -o @out1@""")
		varsPerNormal(key)._filename("out1","out1.vcf")			
		varsPerNormalVCF(key) = varsPerNormal(key).out1
        }

	val varsPerNormalCSV = Array2CSV(in = varsPerNormalVCF)

	val ponOVCActDNA = BashEvaluate(var1 = varsPerNormalCSV,
									var2 = reference, 
									var3 = intervals,
									script = """ 
											 	command='java -Xmx4g -jar /opt/share/gatk-3.7/GenomeAnalysisTK.jar -T CombineVariants -R @var2@ -minN 2 --setKey "null" --filteredAreUncalled --filteredrecordsmergetype KEEP_IF_ANY_UNFILTERED -L @var3@ -o @out1@'
												for i in $( cut @var1@ -f 2 | tr -d '"' | tr -d '\b\r' | awk 'NR>1' )
													do 
														command="${command} -V ${i}"
													done
												eval $command
											 """)
	ponOVCActDNA._filename("out1","ponOVCActDNA.vcf")
}    
