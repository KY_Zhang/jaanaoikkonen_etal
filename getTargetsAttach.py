import sys

_in = open(sys.argv[1])
_in2 = open("/mnt/storageBig7/home/kaizhang/OvCa-ctDNA/20180131/targetGenes_inflated.interval_list")
_out = open(sys.argv[2],"w")

header = _in.readline().strip()
print >> _out, header+"\ttarget\tparID"

for line in _in:
    tumorKey = line.split("\t")[1]
    i = 0
    for line2 in _in2: 
        i = i+1
        print >> _out, line.strip()+"\t"+line2.strip()+"\t"+tumorKey+"_"+str(i)
    _in2.seek(0)        
                
_in.close()
_in2.close()
_out.close()    
