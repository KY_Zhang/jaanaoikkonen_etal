#!/usr/bin/env anduril
//$OPT --threads 120
//$OPT --pipe "tee $ANDURIL_EXECUTION_DIR/_log"
//$OPT --pipe "anduril-pager --ls"
//$OPT --wrapper slurm-prefix

import anduril.builtin._
import anduril.tools._
import anduril.anima._
import anduril.microarray._
import anduril.sequencing._
import org.anduril.runtime._

object variantCall{

    val reference = INPUT(path = "GRCh38.d1.vd1.fa")
	val dbsnp = INPUT(path = "dbsnp_144_sorted.hg38_withChrSorted.vcf")
    val cosmic = INPUT(path = "cosmic82_4mutect_sorted.vcf")
    val pon = INPUT(path = "/mnt/storageBig7/home/kaizhang/OvCa-ctDNA/result_pon/ponOVCActDNA/ponOVCActDNA.vcf")
    val recalBamsCSV1 = INPUT(path = "")
    val recalBamsCSV2 = INPUT(path ="")

	val getPairScript = INPUT(path = "get_matchBams.py")
    val getTargetsAttach = INPUT(path = "getTargetsAttach.py")
    val intervals = INPUT(path = "targetGenes_inflated.interval_list")

	val sampleBamsArray3 = NamedMap[CSV2Array]("sampleBamsArray3")
	val merged3 = NamedMap[BamCombiner]("merged3")
    val varsArray = NamedMap[ArrayConstructor]("varsArray")
    val arrayIn = NamedMap[CSV2Array]("arrayIn")
    val bamArray = NamedMap[CSV2Array]("bamArray")
    val mergedByPatient = NamedMap[BashEvaluate]("mergedByPatient")   
	val varsHC = NamedMap[BashEvaluate]("varsHC")
    val varsHCarray = NamedMap[BinaryFile]("varsHCarray")
	val varsHCexMA = NamedMap[BashEvaluate]("varsHCexMA")
	val varsAnno = NamedMap[Annovar]("varsAnno")
	val varsAnnoTable = NamedMap[BashEvaluate]("varsAnnoTable")
	val varsAnnoTableOut = NamedMap[CSVTransformer]("varsAnnoTableOut")
	val varsAnnoTableFildbSNP = NamedMap[CSVDplyr]("varsAnnoTableFildbSNP")
	val varsAnnoTableFilstrand = NamedMap[CSVDplyr]("varsAnnoTableFilstrand")
	val varsAnnoTableShortFormat = NamedMap[CSVDplyr]("varsAnnoTableShortFormat")
	val variantsDPexonic = NamedMap[CSVTransformer]("variantsDPexonic")
	val variantsDP = NamedMap[CSVTransformer]("variantsDP")
	val variantsDPexonicFilstrand = NamedMap[CSVTransformer]("variantsDPexonicFilstrand")
	val variantsDPFilstrand = NamedMap[CSVTransformer]("variantsDPFilstrand")  

   	val recalBamsCSV1target = REvaluate(table1 = recalBamsCSV1,
                                               script = """
                                                        table.out <- subset(table1, !sapply(strsplit(Key,"_"), `[`, 1) %in% c("HD776","HD777","HD778","H057"))
                                                        """)

    val recalBamsCSV = CSVJoin(in1 = recalBamsCSV1target.table,
                               in2 = recalBamsCSV2,
                               intersection = false)

    val bamsMatched = BashEvaluate(var1 = recalBamsCSV,
                                   var2 = getPairScript,
                                   script = """python @var2@ @var1@ @out1@""")
 
    val bamsMatchedTar = BashEvaluate(var1 = bamsMatched.out1,
                                      var2 = getTargetsAttach,                                      
                                      script = """python @var2@ @var1@ @out1@""") 
   
	val bamTumor = NamedMap[INPUT]("bamTumor")
	val bamNormal = NamedMap[INPUT]("bamNormal")	
	val varsByTar = NamedMap[BashEvaluate]("varsByTar")
	val varsByTarPass = NamedMap[BashEvaluate]("varsByTarPass")
	val varsByTarPassOut = NamedMap[Any]("varsByTarPassOut")	
	val varsByTarMutect2 = NamedMap[Any]("varsByTarMutect2")

    for (row <- iterCSV(bamsMatchedTar.out1)){
		val parID = row("parID")
		val target = row("target")
        bamTumor(parID) = INPUT(row("tumorFile"))
        bamNormal(parID) = INPUT(row("controlFile"))        
        
        varsArray(parID) = ArrayConstructor(file1 = bamTumor(parID),
                                               file2 = bamNormal(parID),
                                               file3 = reference,
                                               file4 = dbsnp,
                                               file5 = cosmic,
                                               file6 = intervals,
                                               file7 = pon,
                                               key1 = "bamTumor",
                                               key2 = "bamNormal",
                                               key3 = "reference",
                                               key4 = "dbsnp",
                                               key5 = "cosmic",
                                               key6 = "intervals",
                                               key7 = "pon")                
         
		varsByTar(parID) = BashEvaluate(vararray = varsArray(parID),
                                        param1=target,
                                        script = """ java -Xmx4g -jar /opt/share/gatk-3.7/GenomeAnalysisTK.jar -T MuTect2 -R $reference --cosmic $cosmic --dbsnp $dbsnp -L @param1@ -I:tumor $bamTumor -I:normal $bamNormal -PON $pon -o @out1@ --max_alt_alleles_in_normal_count 2 --max_alt_alleles_in_normal_qscore_sum 200 --max_alt_allele_in_normal_fraction 0.07 --enable_clustered_read_position_filter""")

        varsByTar(parID)._filename("out1", "out1.vcf")
        varsByTarMutect2(parID) = varsByTar(parID).out1
    
        varsByTarPass(parID) = BashEvaluate(var1 = varsByTar(parID).out1,
                                             script = """
                                                      grep "^#\|PASS" @var1@ > @out1@
                                                      """)
        varsByTarPass(parID)._filename("out1","out1.vcf")
        varsByTarPassOut(parID) = varsByTarPass(parID).out1
        }

	val varsByTarCSV = Array2CSV(in = varsByTarPassOut)
	val varsByTarCSVmerged = CSVDplyr(csv1=varsByTarCSV,
                                   	  csv2=bamsMatchedTar.out1,
	                                  function1="""merge(csv2,by.x=1,by.y="parID",sort=F)""",
    	                              function2="""select(tumorKey,File)""")

	val varsByTarCSVsplit = REvaluate(table1 = varsByTarCSVmerged,
                              	 	  script="""
                                        	 array.out <- split(table1[,-1,drop=F],table1[,1])
	                                         table.out <- data.frame()
                                         """)

	val varsByTarCSVsplitCSV = Array2CSV(in = varsByTarCSVsplit.outArray)
	
	 // Merge the variants from different chromosomes to the same file
	
	val varsBySample = NamedMap[BashEvaluate]("varsBySample")
    val varsBySampleOut = NamedMap[Any]("varsBySampleOut")
    val varsBySampleSorted = NamedMap[QuickBash]("varsBySampleSorted")
    val varsBySampleCleaned = NamedMap[QuickBash]("varsBySampleCleaned")
	
	for ( row <- iterCSV(varsByTarCSVsplitCSV) ) {
	   val key = row("Key")
	   varsBySample(key) = BashEvaluate(var1=varsByTarCSVsplit.outArray(key),
	                                    var2=reference,
    		                            script="""
             	                         command="java -cp /opt/share/gatk-3.7/GenomeAnalysisTK.jar org.broadinstitute.gatk.tools.CatVariants -R @var2@ -out @out1@"
                	                      for i in $( cat @var1@ | tr -d '"' | awk 'NR>1' )
                    	                    do
                        	                  command="${command} -V ${i}"
                            	            done
                                	      eval $command                                        
                                    	  """)
	  varsBySample(key)._filename("out1","out1.vcf")

      varsBySampleSorted(key) = QuickBash(in = varsBySample(key).out1,
                                          script = """vcfstreamsort $in > $out""")

      varsBySampleCleaned(key) = QuickBash(in = varsBySampleSorted(key).out,
                                           script = """vcfuniq $in > $out""")

	  varsBySampleOut(key) = varsBySampleCleaned(key).out
 }
   
    val varsBySamplePassCSV = Array2CSV(in = varsBySampleOut)
    val varsBySampleSplitByPatient = REvaluate(table1 = varsBySamplePassCSV,
                                               script = """
                                                        library(tidyr)
                                                        library(dplyr)
                                                        table.out <- table1 %>% extract(Key,into="Patient",remove=F)
                                                        array.out <- split(table.out[,c("Key","File")],table.out$Patient)
                                                        """)
    val varsByPatient = Array2CSV(in = varsBySampleSplitByPatient.outArray)

    val bamsByPatient = REvaluate(table1 = recalBamsCSV,
                                  script = """
                                           library(tidyr)
                                           library(dplyr)
                                           table.out <- table1 %>% extract(Key,into="Patient",remove=F)
                                           array.out <- split(table.out[,c("Key","File")],table.out$Patient)
                                           """) 
    
    // merge all VCFs from the same patient 
    for (row <- iterCSV(varsByPatient)){
        val patient = row("Key")
        arrayIn(patient) = CSV2Array(in = varsBySampleSplitByPatient.outArray(patient),
                                     keys = "column")
        bamArray(patient) = CSV2Array(in = bamsByPatient.outArray(patient),
                                      keys = "column")
        mergedByPatient(patient) = BashEvaluate(var1 = reference,
                                                array1 = arrayIn(patient),
                                                script = "java -Xmx4g -jar /opt/share/gatk-3.7/GenomeAnalysisTK.jar -T CombineVariants -R @var1@ -o @out1@ -genotypeMergeOptions UNIQUIFY " +
                                             """ $( paste -d ' ' <(getarraykeys array1) <(getarrayfiles array1)  | sed 's,^, --variant:,' | tr -d '\\\n' ) --disable_auto_index_creation_and_locking_when_reading_rods""" )
		mergedByPatient(patient)._filename("out1","out1.vcf")

		varsHC(patient) = BashEvaluate(var1 = reference, 
			 						  var2 = mergedByPatient(patient).out1,
			 						  var3 = dbsnp,
									  array1 = bamArray(patient),
									  script = """
											   java -Xmx4g -jar /opt/share/gatk-3.7/GenomeAnalysisTK.jar -T HaplotypeCaller -R @var1@ -stand_call_conf 0 --dbsnp @var3@ -A StrandAlleleCountsBySample -gt_mode GENOTYPE_GIVEN_ALLELES -out_mode EMIT_ALL_SITES -alleles @var2@  -mmq 0 -o @out1@ $( getarrayfiles array1  | sed 's,^, -I ,' | tr -d '\\\n' ) --disable_auto_index_creation_and_locking_when_reading_rods""" )	
		varsHC(patient)._filename("out1","out1.vcf")
        varsHCarray(patient) = varsHC(patient).out1
		
		// filter out triallelic site (in Mutect, this is automatically done)


		// annotation, includes both snps and indels
        varsAnno(patient) = Annovar(vcfIn = varsHC(patient).out1,
                                    annovarPath="/opt/share/annovar_20160201/annovar",
                                    annovardb="/mnt/csc-gc7/resources/annovardb/",
                                    buildver="hg38",
                                    inputType="vcf",
                                    protocol="wgEncodeGencodeBasicV24,refGene,cytoBand,avsnp147,kaviar_20150923,cosmic82,clinvar_20150629,dbnsfp30a",
                                    operation="g,g,r,f,f,f,f,f",                                 
                                    threads = 6)
		
        varsAnnoTable(patient) = BashEvaluate(var1 = varsAnno(patient).vcfOut,
		                                      var2 = reference,
         			                          script = """java -Xmx8g -jar /opt/share/gatk-3.7/GenomeAnalysisTK.jar -T VariantsToTable -R @var2@ --allowMissingData -V @var1@ -o @out1@ -F CHROM -F POS -F REF -F ALT -F ID -F Filter -F cytoBand -F Func.refGene -F Gene.refGene -F GeneDetail.refGene -F ExonicFunc.refGene -F AAChange.refGene \
                                              -F Func.wgEncodeGencodeBasicV24 -F Gene.wgEncodeGencodeBasicV24 -F GeneDetail.wgEncodeGencodeBasicV24 -F ExonicFunc.wgEncodeGencodeBasicV24 \
                                              -F AAChange.wgEncodeGencodeBasicV24 -F avsnp147 -F cosmic82 \
                                              -F clinvar_20150629 -F Kaviar_AF -F Kaviar_AC -F Kaviar_AN -F SIFT_score -F SIFT_pred -F Polyphen2_HDIV_score -F Polyphen2_HDIV_pred \
                                              -F Polyphen2_HVAR_score -F Polyphen2_HVAR_pred -F MutationTaster_score -F MutationTaster_pred -F MutationAssessor_score -F MutationAssessor_pred \
                                              -F CADD_phred -F DANN_score -GF AD -GF DP -GF SAC -F FS """)

	    varsAnnoTable(patient)._filename("out1","variants_annotated.table")        

      	// remove variants without variant allele reads in any sample
        // add VAF
		varsAnnoTableOut(patient) = CSVTransformer(csv1=varsAnnoTable(patient).out1,
    	                                           transform1="""
	                                                      csv <- as.data.frame(csv1[,grepl("\\.AD",colnames(csv1))])
                                                          VA <- apply(csv,2,function(x)as.numeric(sapply(strsplit(x,","),function(x)x[2])))
                                                          RA <- apply(csv,2,function(x)as.numeric(sapply(strsplit(x,","),function(x)x[1])))
                                                          colnames(VA) <- gsub("\\.AD","",colnames(VA))
                                                          colnames(RA) <- gsub("\\.AD","",colnames(RA))
                                                          AF <- VA / (VA + RA)
                                                          colnames(AF) <- paste0(colnames(AF),".AF")
                                                          AF <- apply(AF,2,function(x)round(x,3))
                                                          csv1 <- data.frame(csv1,AF)
                                                          normal <- as.data.frame(AF[,grepl("\\_normal",colnames(AF))])
                                                          csv1$nSamples <- rowSums(AF>0)-rowSums(normal>0)
                                                          idx <- rowSums(VA,na.rm=T)>0
                                                          csv1[idx,]
                                                          """)			
        
     // filter variants in dbSNP except those in cosmic
  		varsAnnoTableFildbSNP(patient) = CSVDplyr(csv1 = varsAnnoTableOut(patient),
	    	                                      function1 = """filter(avsnp147 == "." |  cosmic82 != ".")""")   	


	// variants should have at least 1 read in each strand for variant allele
    // filter strand bias based on fisher test FS>20
  
  		varsAnnoTableFilstrand(patient) = CSVDplyr(csv1 = varsAnnoTableFildbSNP(patient),
        			                                 script = """source("/mnt/storageBig7/amjad/OVCA/hercules/strandFilter.R")""",
                    			                     //function1 = """filter(rowSums(select(csv1, ends_with(".AF") )>0.05)>0)""",
                                			         function1 = """filterStrand()""")
		
				                                         
		varsAnnoTableShortFormat(patient) = CSVDplyr(csv1= varsAnnoTableFildbSNP(patient),
                                                 script="library(tidyr)",
                                                 function1="""mutate(samples = paste(gsub("\\.AD","",colnames(csv1)[grepl("\\.AD",colnames(csv1))]),collapse=";"))""",
                                                 function2="""unite("readCounts",ends_with("AD"),sep=";")""",
                                                 function3="""select(-ends_with("DP"))""",
                                                 function4="""select(-ends_with("AD"))""",
                                                 function5="""select(-ends_with("AF"))""")

		variantsDPexonic(patient) = CSVTransformer(csv1=varsAnnoTableFildbSNP(patient),
                                       transform1="""
                                                  csv1 <- csv1[csv1$Func.refGene %in% "exonic",]
                                                  csv <- csv1[,grepl("\\.AD",colnames(csv1))]
                                                  VA <- apply(csv,2,function(x)as.numeric(sapply(strsplit(x,","),function(x)x[2])))
                                                  RA <- apply(csv,2,function(x)as.numeric(sapply(strsplit(x,","),function(x)x[1])))
                                                  colnames(VA) <- gsub("\\.AD","",colnames(VA))
                                                  colnames(RA) <- gsub("\\.AD","",colnames(RA))
                                                  csv <- VA / (VA + RA)
                                                  csv <- na.omit(csv)
                                                  csv[rowSums(csv)>0,]
                                                  """)

		variantsDP(patient) = CSVTransformer(csv1=varsAnnoTableFildbSNP(patient),
                    			             transform1="""
                                                  csv <- csv1[,grepl("\\.AD",colnames(csv1))]
                                                  VA <- apply(csv,2,function(x)as.numeric(sapply(strsplit(x,","),function(x)x[2])))
                                                  RA <- apply(csv,2,function(x)as.numeric(sapply(strsplit(x,","),function(x)x[1])))
                                                  colnames(VA) <- gsub("\\.AD","",colnames(VA))
                                                  colnames(RA) <- gsub("\\.AD","",colnames(RA))
                                                  csv <- VA / (VA + RA)
                                                  csv <- na.omit(csv)
                                                  csv[rowSums(csv)>0,]
                                                  """)

		variantsDPexonicFilstrand(patient) = CSVTransformer(csv1=varsAnnoTableFilstrand(patient),
                                       transform1="""
                                                  csv1 <- csv1[csv1$Func.refGene %in% "exonic",]
                                                  csv <- csv1[,grepl("\\.AD",colnames(csv1))]
                                                  VA <- apply(csv,2,function(x)as.numeric(sapply(strsplit(x,","),function(x)x[2])))
                                                  RA <- apply(csv,2,function(x)as.numeric(sapply(strsplit(x,","),function(x)x[1])))
                                                  colnames(VA) <- gsub("\\.AD","",colnames(VA))
                                                  colnames(RA) <- gsub("\\.AD","",colnames(RA))
                                                  csv <- VA / (VA + RA)
                                                  csv <- na.omit(csv)
                                                  csv[rowSums(csv)>0,]
                                                  """)

		variantsDPFilstrand(patient) = CSVTransformer(csv1=varsAnnoTableFilstrand(patient),
                    			             transform1="""
                                                  csv <- csv1[,grepl("\\.AD",colnames(csv1))]
                                                  VA <- apply(csv,2,function(x)as.numeric(sapply(strsplit(x,","),function(x)x[2])))
                                                  RA <- apply(csv,2,function(x)as.numeric(sapply(strsplit(x,","),function(x)x[1])))
                                                  colnames(VA) <- gsub("\\.AD","",colnames(VA))
                                                  colnames(RA) <- gsub("\\.AD","",colnames(RA))
                                                  csv <- VA / (VA + RA)
                                                  csv <- na.omit(csv)
                                                  csv[rowSums(csv)>0,]
                                                  """)
	}

	val varsHeatmaps = REvaluate(inArray= variantsDP,
                           script="""
                                  library(pheatmap)
                                  library(viridis)
                                  setwd(document.dir)
                                  for( i in names(array)){
                                    freq <- array[[i]]
                                    bin <- freq
                                    bin[bin>0] <- 1
                                    rownames(bin) <- paste0("snv",c(1:nrow(bin)))
                                    rownames(freq) <- paste0("snv",c(1:nrow(freq)))
                                    bin <- bin[do.call(order,bin),]
                                    bin <- as.data.frame(apply(bin,2,rev))
                                    freq <- freq[rownames(bin),]
                                    tbin <- t(bin)
                                    tfreq <- t(freq)
                                    pheatmap(tbin,show_rownames=T,show_colnames=F,cluster_rows=F,cellheight=10,cellwidth=3,color=c("grey85","#4575B4"),
                                             border_color="white",cluster_cols=F,filename=paste0(i,"_heatmap.pdf"))
                                    pheatmap(tfreq,show_rownames=T,show_colnames=F,cluster_rows=F,cellheight=10,cellwidth=3,color=rev(magma(12)),
                                             breaks=c(0,0.03,0.06,0.1,0.15,0.20,0.25,0.30,0.35,0.45,1),cluster_cols=F,filename=paste0(i,"_vaf_heatmap.pdf"))
                                                              }
                                                    table.out <- data.frame()
                                  """)

	
	val varsHeatmapsFilstrand = REvaluate(inArray= variantsDPFilstrand,
                           script="""
                                  library(pheatmap)
                                  library(viridis)
                                  setwd(document.dir)
                                  for( i in names(array)){
                                    freq <- array[[i]]
                                    bin <- freq
                                    bin[bin>0] <- 1
                                    rownames(bin) <- paste0("snv",c(1:nrow(bin)))
                                    rownames(freq) <- paste0("snv",c(1:nrow(freq)))
                                    bin <- bin[do.call(order,bin),]
                                    bin <- as.data.frame(apply(bin,2,rev))
                                    freq <- freq[rownames(bin),]
                                    tbin <- t(bin)
                                    tfreq <- t(freq)
                                    pheatmap(tbin,show_rownames=T,show_colnames=F,cluster_rows=F,cellheight=10,cellwidth=3,color=c("grey85","#4575B4"),
                                             border_color="white",cluster_cols=F,filename=paste0(i,"_heatmap.pdf"))
                                    pheatmap(tfreq,show_rownames=T,show_colnames=F,cluster_rows=F,cellheight=10,cellwidth=3,color=rev(magma(12)),
                                             breaks=c(0,0.03,0.06,0.1,0.15,0.20,0.25,0.30,0.35,0.45,1),cluster_cols=F,filename=paste0(i,"_vaf_heatmap.pdf"))
                                                              }
                                                    table.out <- data.frame()
                                  """)


	val varsFolder = Array2Folder(in = varsAnnoTableOut,
								  fileMode = "@key@.csv")
    val varsFildbSNPFolder = Array2Folder(in = varsAnnoTableFildbSNP,
                                  fileMode = "@key@.csv")
	val varsFilstrandFolder = Array2Folder(in = varsAnnoTableFilstrand,
                     		               fileMode = "@key@.csv")

    val allVars = CSVListJoin(in = varsAnnoTableShortFormat)

    OUTPUT(makeArray(varsHCarray))

}    
